from flask import Flask, request, render_template
from model import get_model, predict
import numpy as np


app = Flask(__name__)


@app.route("/predict")
def get_prediction():
    smn1 = np.array([[68., 2800.]])
    prediction = predict(smn1)
    return str(prediction)


@app.route('/user/<name>/')
def user_profile(name):
    return f"Profile page of user {name}"


@app.route('/')
def index():
    model = get_model()
    return model


@app.route('/predict_form/', methods=['post', 'get'])
def login():
    message = ''
    if request.method == 'POST':
        data = request.form.get('username')


    return render_template('login.html', message=message)


if __name__ == '__main__':
    app.run()
