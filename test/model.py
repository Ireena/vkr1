import os
import tensorflow
from tensorflow import keras


def predict(smn):
    model = keras.models.load_model("models/model")
    prediction = model.predict(smn)[0][0]

    return prediction


def get_model():
    output = str(os.listdir("models/model"))

    return output
