
from tensorflow import keras


def predict(smn):
    model = keras.models.load_model("models/model_SMN")
    prediction = model.predict(smn)[0][0]

    return prediction

