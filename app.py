from flask import Flask, request, render_template
from model import predict
import numpy as np


app = Flask(__name__)


@app.route("/predict/")
def get_prediction(smn):
    prediction = predict(smn)
    return str(prediction)


@app.route('/')
def index():
    return "main"


@app.route('/predict_form/', methods=['post', 'get'])
def login():
    message = ''
    if request.method == 'POST':
        smn = request.form.get('username')
        smn_parameters = smn.split(" ")
        smn = [float(param) for param in person_parameters]
        smn = np.array([smn])

        message = get_prediction(smn)

    return render_template('login.html', message=message)


if __name__ == '__main__':
    app.run()
